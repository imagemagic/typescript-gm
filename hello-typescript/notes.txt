TypeScript: The Future of Declaration Files
https://blogs.msdn.microsoft.com/typescript/2016/06/15/the-future-of-declaration-files/


// [1]
npm install @types/node --save-dev

// [2]
declare var require: any

// [3]
import gm from 'gm' // or
import gm = require('gm')

