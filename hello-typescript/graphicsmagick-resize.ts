var fs = require('fs'),
    gm = require('gm');

gm('graphicsmagick-logo.png')
.resize(240, 240)
.noProfile()
.write('graphicsmagick-resized.png', function (err) {
  if (!err) console.log('done');
});

